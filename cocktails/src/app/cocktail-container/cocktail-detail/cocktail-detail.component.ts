import { Component, OnInit, Input } from '@angular/core'
import { Cocktail } from 'src/app/shared/interfaces/cocktails.interfaces'

@Component({
  selector: 'app-cocktail-details',
  templateUrl: './cocktail-detail.component.html',
  styleUrls: ['./cocktail-detail.component.scss'],
})
export class CocktailDetailsComponent implements OnInit {
  @Input() cocktail!: Cocktail

  constructor() {}

  ngOnInit(): void {}
}
