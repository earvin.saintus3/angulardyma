import { Ingredient } from './ingredient.interfaces'

export interface Cocktail {
  name: string
  img: string
  description: string
  ingredients?: Ingredient[]
}
