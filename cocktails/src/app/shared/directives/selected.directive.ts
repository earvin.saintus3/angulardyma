import { Directive, HostBinding, Input, OnChanges } from '@angular/core'

@Directive({
  selector: '[appSelected]',
})
export class SelectedDirective {
  @Input() public appSelected?: boolean
  @HostBinding('style.backgroundColor') private backgroundColor: string
  @HostBinding('style.color') private color: string
  @HostBinding('style.fontWeight') private fontWeight: string

  ngOnChanges(): void {
    if (this.appSelected) {
      this.backgroundColor = 'var(--primary)'
      this.color = '#fff'
      this.fontWeight = '500'
    } else {
      this.backgroundColor = '#fff'
      this.color = 'var(--text-regular)'
      this.fontWeight = '400'
    }
  }

  constructor() {
    this.appSelected = false
    this.backgroundColor = '#fff'
    this.fontWeight = '400'
    this.color = 'var(--text-regular)'
  }
}
